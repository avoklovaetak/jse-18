package ru.volkova.tm.api.service;

import ru.volkova.tm.entity.User;

public interface IAuthService {

    User getUser();

    void setCurrentUserId(User user);

    String getUserId();

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
