package ru.volkova.tm.api.service;

import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTasksByProjectId(String projectId);

    Task bindTaskByProjectId(String projectId, String taskId);

    Task unbindTaskByProjectId(String projectId, String taskId);

    Project removeProjectById(String id);

}
