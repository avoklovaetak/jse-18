package ru.volkova.tm.api.service;

import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User findById(String id);

    boolean isLoginExists(String login);

    User removeUser(User user);

    User findByLogin(String login);

    boolean isEmailExists(String email);

    User removeById(String id);

    User removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(
            final String userId,
            final String firstName,
            final String secondName,
            final String middleName
    );

}
