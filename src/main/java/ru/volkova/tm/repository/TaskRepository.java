package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(list);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task: list) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        list.remove(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task: list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task: list){
            if (projectId.equals(task.getProjectId())) projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public void removeAllByProjectId(String projectId) {
        list.removeIf(task -> projectId.equals(task.getProjectId()));
    }

    @Override
    public Task bindTaskByProjectId(String projectId, String taskId) {
        final Task task = findOneById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskByProjectId(String projectId, String taskId) {
        final Task task = findOneById(taskId);
        task.setProjectId(null);
        return task;
    }
}
